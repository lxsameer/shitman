# -----------------------------------------------------------------------------
#    Shitman
#    Copyright (C) 2011  Sameer Rahmani <lxsameer@gnu.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# -----------------------------------------------------------------------------

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol

from signals import signal, SignalBase


class Ability (object):
    """
    Ability is the base class for all the abilities that shitman will support.
    """
    def __init__(self):
        pass

    def init_environment(self):
        pass


class ShitMan (irc.IRCClient):
    """
    This class is the base class for any bot you will use later, you can use
    this as a stand alone bot too.
    """
    #: Nickname the bot will use.
    nickname = None

    #: Password used to log on to the server. May be None.
    password = None

    #: Supplied to the server during login as the "Real name"
    #: or "ircname". May be None.
    realname = None

    #: Supplied to the server during login as the "User name". May be None.
    username = None

    #: Sent in reply to a USERINFO CTCP query. If None, no USERINFO
    #: reply will be sent. "This is used to transmit a string which is
    #: settable by the user (and never should be set by the client)."
    userinfo = None

    #: CTCP VERSION reply, client name. If None, no VERSION reply will be sent.
    versionName = "Shitman"

    #: CTCP VERSION reply, client name. If None, no VERSION reply will be
    #: sent.
    versionNum = "2.0"

    #: CTCP VERSION reply, client name. If None, no VERSION reply will be sent.
    sourceURL = "https://bitbucket.org/lxsameer/shitman/overview"

    #: Minimum delay between lines sent to the server. If None, no delay will
    #: be imposed. (type: Number of Seconds.)
    lineRate = 3

    #: This propery is a list of ability modules to load for bot instanse
    abilities = []

    #: A list contains all channels to join
    channels = []

    #: Target irc server to connect
    server = "irc.freenode.net"
    port = 6667

    #: Bot profile url
    profile_url = ""

    def __init__(self):
        """
        Load and initialize all the abilities and use them after connecting to
        a irc server and joining to all channels.
        """
        def ability_import(abl):
            __import__(abl, globals(), locals(),
                       [], -1)

        map(ability_import, self.abilities)

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        signal.send(self.ConnectionMade)
        print "here"

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        signal.send(self.ConnectionLost)
        print "Asdasd"

    # callbacks for events
    def signedOn(self):
        """
        Called when bot has succesfully signed on to server.
        """
        map(self.join, self.channels)

    def joined(self, channel):
        """
        This will get called when the bot joins the channel.
        """
        self.say(channel, "Hi all, %s is here." % self.nickname)

    def privmsg(self, user, channel, msg_):
        """
        This will get called when the bot receives a message.
        """
        pass

    def action(self, user, channel, msg):
        """
        This will get called when the bot sees someone do an action.
        """
        pass

    def alterCollidedNick(self, nickname):
        """
        Generate an altered version of a nickname that caused a collision in an
        effort to create an unused related name for subsequent registration.
        """
        return nickname + '_'

    # Signals
    class ConnectionMade(SignalBase):
        name = "connection_made"

    class ConnectionLost(SignalBase):
        name = "connection_lost"


class ShitManFactory(protocol.ClientFactory):
    """
    A factory for Shitmans.
    """

    def __init__(self, shitman):
        """
        shitman is a subclass of ShitMan class.
        """
        self.protocol = shitman

    def clientConnectionLost(self, connector, reason):
        """
        If we get disconnected, reconnect to server.
        """
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        reactor.stop()

    def buildProtocol(self, addr):
        self.instance = self.protocol()
        return self.instance
