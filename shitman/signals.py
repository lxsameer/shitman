# -----------------------------------------------------------------------------
#    Shitman
#    Copyright (C) 2011  Sameer Rahmani <lxsameer@gnu.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# -----------------------------------------------------------------------------


class SignalBase(object):
    """
    Signal base class. Each signal must subclass from this class.
    """
    name = ""
    _slots = {}

    def run_slots(self, *args, **kwargs):
        for slot.__self__ in self._slots:
            print ">>> ", slot, self.__class__.__name__
            slot(*args, **kwargs)

    def register(self, slot):
        print "1", slot
        if not slot.__self__ in self._slots:
            print "2"
            self._slots.append(slot)

    def pp(self):
        return self._slots


class SignalHandler(object):
    """
    SignalHandler class is in charge of slot registration for a signal.
    """
    _signals = {}

    def register(self, signal, slot):
        """
        Register a new slot for given signal.
        """
        if not issubclass(signal, SignalBase):
            raise TypeError("'signal' should be a Signal instance")

        if signal.__class__.__name__ in self._signals:
            self._signals[signal.__class__.__name__].register(slot)
        else:
            self._signals[signal.__class__.__name__] = signal()
            self._signals[signal.__class__.__name__].register(slot)
        print "=> ", self._signals, map(lambda x: self._signals[x].pp(), self._signals)

    def send(self, signal, *args, **kwargs):
        if signal.__class__.__name__ in self._signals:
            self._signals[signal.__class__.__name__].run_slots(*args, **kwargs)


signal = SignalHandler()
